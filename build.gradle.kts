import com.github.gmazzo.gradle.plugins.tasks.BuildConfigTask

plugins {
    kotlin("jvm")

    // https://github.com/gmazzo/gradle-buildconfig-plugin
    id("com.github.gmazzo.buildconfig") version "1.5.0"
}

group = "vanadis.vanadis.core"
version = "1.0.0"

dependencies {

    implementation(kotlin("stdlib"))

    implementation(kotlin("reflect"))

    implementation(project(":common:logger"))

    /**
     * Javax annotations (Removed from JDK science 8 version)
     */

    // https://mvnrepository.com/artifact/javax.annotation/javax.annotation-api
    api("javax.annotation", "javax.annotation-api", "1.3.2")

    /**
     * Inversion of control (IOC)
     */

    // https://mvnrepository.com/artifact/org.springframework.boot/spring-boot-starter
    api("org.springframework.boot", "spring-boot-starter", "2.1.7.RELEASE")

    /**
     * Reflections
     */

    // https://mvnrepository.com/artifact/org.reflections/reflections
    implementation("org.reflections", "reflections", "0.9.11")

    /**
     * Data serialization/deserialization
     */

    // https://mvnrepository.com/artifact/com.fasterxml.jackson.core/jackson-core
    implementation("com.fasterxml.jackson.core", "jackson-core", "2.9.9")

    // https://mvnrepository.com/artifact/com.fasterxml.jackson.core/jackson-annotations
    implementation("com.fasterxml.jackson.core", "jackson-annotations", "2.9.9")

    // https://mvnrepository.com/artifact/com.fasterxml.jackson.core/jackson-databind
    implementation("com.fasterxml.jackson.core", "jackson-databind", "2.9.9")

    // https://mvnrepository.com/artifact/com.fasterxml.jackson.dataformat/jackson-dataformat-yaml
    implementation("com.fasterxml.jackson.dataformat", "jackson-dataformat-yaml", "2.9.9")
}

// https://github.com/gmazzo/gradle-buildconfig-plugin
buildConfig {
    className("CoreConfig")
    packageName("com.vanadis.config")

    buildConfigField("String", "APP_NAME", "\"${project.name}\"")
    buildConfigField("String", "VERSION", "\"${project.version}\"")
    buildConfigField("long", "BUILD_TIME", "${System.currentTimeMillis()}L")
}

tasks.withType(BuildConfigTask::class) {
    addGeneratedAnnotation = false
}