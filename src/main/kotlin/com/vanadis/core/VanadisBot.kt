package com.vanadis.core

import com.vanadis.logger.logback.logger
import org.springframework.boot.SpringApplication
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration
import kotlin.reflect.KClass

@Configuration
@ComponentScan("com.vanadis")
open class VanadisBot {

    @Bean
    open fun mainClass(): KClass<*> {
        return mainClass
    }

    companion object {

        @JvmStatic
        private val log = logger()

        @JvmStatic
        private lateinit var mainClass: KClass<*>

        /**
         * Vanadis bot entry point.
         * Method used to start application.
         *
         * Overload for Java.
         */
        @JvmStatic
        fun launch(mainClass: Class<*>, args: Array<String>) {
            launch(mainClass.kotlin, args)
        }

        /**
         * Vanadis bot entry point.
         * Method used to start application.
         */
        @JvmStatic
        fun launch(mainClass: KClass<*>, args: Array<String>) {
            this.mainClass = mainClass

            SpringApplication.run(arrayOf(mainClass.java, VanadisBot::class.java), args)
        }
    }
}
