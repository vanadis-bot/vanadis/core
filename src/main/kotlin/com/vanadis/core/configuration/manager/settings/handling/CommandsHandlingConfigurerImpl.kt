package com.vanadis.core.configuration.manager.settings.handling

import com.vanadis.core.configuration.manager.settings.ManagerConfigurer
import com.vanadis.core.command.provider.CommandProvider
import com.vanadis.core.command.provider.DefaultCommandProvider
import java.util.*
import kotlin.collections.ArrayList
import kotlin.reflect.KClass

class CommandsHandlingConfigurerImpl(
        private val managerConfigurer: ManagerConfigurer
) : CommandsHandlingConfigurer {

    private val commandAnnotations: ArrayList<KClass<out Annotation>> = ArrayList()

    var commandsManager: KClass<*>? = null
        private set

    var commandClass: KClass<*>? = null
        private set

    var commandProvider: KClass<out CommandProvider<*>> = DefaultCommandProvider::class
        private set

    override fun addCommandAnnotation(annotationClass: KClass<out Annotation>): CommandsHandlingConfigurer {
        commandAnnotations.add(annotationClass)

        return this
    }

    override fun addCommandAnnotation(vararg annotationClass: KClass<out Annotation>): CommandsHandlingConfigurer {
        commandAnnotations.addAll(annotationClass)

        return this
    }

    override fun setCommandProvider(commandProvider: KClass<out CommandProvider<*>>): CommandsHandlingConfigurer {
        this.commandProvider = commandProvider

        return this
    }

    override fun setManager(commandsManager: KClass<*>): CommandsHandlingConfigurer {
        return setManager(commandsManager, Any::class)
    }

    override fun setManager(commandsManager: KClass<*>, commandClass: KClass<*>): CommandsHandlingConfigurer {
        this.commandsManager = commandsManager
        this.commandClass = commandClass

        return this
    }

    override fun and(): ManagerConfigurer {
        return managerConfigurer
    }

    fun createMeta(): ManagerMeta? {
        val commandsManager = this.commandsManager
        val commandClass = this.commandClass
        val commandAnnotations = Collections.unmodifiableList(this.commandAnnotations)

        if (commandsManager != null && commandClass != null && commandAnnotations.size > 0) {
            return ManagerMeta(commandsManager, commandClass, commandProvider, commandAnnotations)
        }

        return null
    }

    data class ManagerMeta(
            val commandsManager: KClass<*>,
            val commandClass: KClass<*>,
            val commandProvider: KClass<out CommandProvider<*>>,
            val commandsAnnotations: List<KClass<out Annotation>>
    )
}