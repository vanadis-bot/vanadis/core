package com.vanadis.core.configuration.manager

import com.vanadis.core.configuration.manager.events.ManagerEventsProvider
import com.vanadis.core.configuration.manager.settings.ManagerConfigurer
import com.vanadis.core.configuration.manager.settings.ManagerConfigurerImpl
import com.vanadis.core.configuration.manager.settings.handling.CommandsHandlingConfigurer
import com.vanadis.core.configuration.manager.settings.handling.CommandsHandlingConfigurerImpl
import com.vanadis.logger.logback.logger
import org.springframework.beans.factory.config.AutowireCapableBeanFactory
import org.springframework.beans.factory.config.BeanPostProcessor
import org.springframework.context.ApplicationContext
import org.springframework.context.ApplicationContextAware
import org.springframework.context.event.ContextRefreshedEvent
import org.springframework.context.event.EventListener
import java.lang.RuntimeException
import javax.annotation.PostConstruct
import kotlin.reflect.KClass

/**
 * Configurer that allows to setup custom manager class for bot commands.
 *
 * Used to detect annotated command beans and create skeleton for them.
 * After skeleton initialization will provide wrapped command classes into manager.
 *
 * @author Dmitry Boldak
 * @since 07.10.2019
 */
abstract class CommandsManagerConfigurer : BeanPostProcessor, ApplicationContextAware {

    /**
     * Manager settings serialization class.
     * Used to setup manager properties using functions chain.
     */
    private val managerConfigurer: ManagerConfigurerImpl = ManagerConfigurerImpl()

    /**
     * Result of configuration using method chains.
     *
     * @see ManagerConfigurer
     * @see ManagerConfigurerImpl
     * @see CommandsHandlingConfigurer
     * @see CommandsHandlingConfigurerImpl
     */
    private val managersSettings: List<CommandsHandlingConfigurerImpl.ManagerMeta>

    /**
     * Fast structure to delegate each class annotated with specific annotation
     * to configured in [configure] CommandsManager. Different key's can reference the same Manager.
     *
     * Note: Different annotations can point into the same Manager realization.
     * Note: One annotation can point to exactly one Manager otherwise you will have Exception in [postInit].
     */
    private val annotationsMappings = HashMap<KClass<out Annotation>, ManagerEventsProvider>()

    /**
     * Manager methods provider. Used to simplify interaction with custom manager implementation.
     */
    private val managerEventsProviders = ArrayList<ManagerEventsProvider>()

    private lateinit var applicationContext: ApplicationContext

    companion object {

        @JvmStatic
        private val log = logger()
    }

    init {
        this.configure(managerConfigurer)
        this.managersSettings = managerConfigurer.getManagersMeta()
    }

    override fun setApplicationContext(applicationContext: ApplicationContext) {
        this.applicationContext = applicationContext
    }

    /**
     * Autowired method that will be called on [CommandsManagerConfigurer] bean initialization layer.
     * Will create realization for configured in [configure] Manager beans using [AutowireCapableBeanFactory].
     * After Manager bean initialization will create mapping in [annotationsMappings] to future Command beans
     * delegation directly into configured Manager bean.
     */
    @PostConstruct
    private fun postInit() {
        val autowireCapableBeanFactory = applicationContext.autowireCapableBeanFactory

        managersSettings.forEach { managerMeta ->
            val commandsManager = managerMeta.commandsManager
            val commandsAnnotations = managerMeta.commandsAnnotations
            val commandProvider = managerMeta.commandProvider

            log.debug("Autowiring instance of command manager: $managerMeta")

            val managerInstance = autowireCapableBeanFactory.getBean(commandsManager.java)

            log.debug("Configured relation: ${commandsManager.qualifiedName} ($managerInstance) -> $commandsAnnotations")

            val eventsProvider = ManagerEventsProvider(
                    managerMeta,
                    managerInstance,
                    commandProvider,
                    autowireCapableBeanFactory
            )

            managerEventsProviders.add(eventsProvider)

            eventsProvider.notifyBeforeCommandsInit()

            commandsAnnotations.forEach { commandsAnnotation ->
                if (annotationsMappings.put(commandsAnnotation, eventsProvider) != null) {
                    throw RuntimeException("Error multiple annotation binding found! You can't bind same annotation " +
                            "to multiple manager classes! Annotation '${commandsAnnotation.simpleName}'.")
                }
            }
        }
    }

    /**
     * Method providing Worker configuration functional to class realization.
     *
     * @param managerConfigurer ManagerConfigurer Function chain based configuration creator
     */
    abstract fun configure(managerConfigurer: ManagerConfigurer)

    /**
     * After all beans initialization event
     */
    @EventListener
    fun handleContextRefresh(event: ContextRefreshedEvent) {
        managerEventsProviders.forEach { eventsProvider ->
            eventsProvider.notifyAllCommandsInitialized()

            eventsProvider.notifyAfterCommandsInit()
        }
    }

    /**
     * Event before bean initialization
     */
    override fun postProcessBeforeInitialization(bean: Any, beanName: String): Any? {
        val beanAnnotations = bean::class.annotations
                .map { annotation -> annotation.annotationClass }

        annotationsMappings.forEach { (annotation, eventsProvider) ->
            if (beanAnnotations.contains(annotation)) {
                eventsProvider.notifyBeforeCommandInitialization(bean)
            }
        }

        return super.postProcessBeforeInitialization(bean, beanName)
    }

    /**
     * Event after bean initialization
     */
    override fun postProcessAfterInitialization(bean: Any, beanName: String): Any? {
        val beanAnnotations = bean::class.annotations
                .map { annotation -> annotation.annotationClass }

        annotationsMappings.forEach { (annotation, eventsProvider) ->
            if (beanAnnotations.contains(annotation)) {
                eventsProvider.notifyAfterCommandInitialization(bean)
            }
        }

        return super.postProcessAfterInitialization(bean, beanName)
    }
}