package com.vanadis.core.configuration.manager.settings

import com.vanadis.core.configuration.manager.settings.handling.CommandsHandlingConfigurer

interface ManagerConfigurer {

    fun commandsHandling(): CommandsHandlingConfigurer
}