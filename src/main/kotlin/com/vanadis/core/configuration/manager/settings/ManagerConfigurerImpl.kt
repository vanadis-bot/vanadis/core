package com.vanadis.core.configuration.manager.settings

import com.vanadis.core.configuration.manager.settings.handling.CommandsHandlingConfigurer
import com.vanadis.core.configuration.manager.settings.handling.CommandsHandlingConfigurerImpl
import com.vanadis.logger.logback.logger

internal class ManagerConfigurerImpl : ManagerConfigurer {

    private val configurers: ArrayList<CommandsHandlingConfigurerImpl> = ArrayList()

    companion object {

        @JvmStatic
        private val log = logger()
    }

    override fun commandsHandling(): CommandsHandlingConfigurer {
        val managerConfigurer = CommandsHandlingConfigurerImpl(this)

        configurers.add(managerConfigurer)

        return managerConfigurer
    }

    internal fun getManagersMeta(): List<CommandsHandlingConfigurerImpl.ManagerMeta> {
        val validMeta: ArrayList<CommandsHandlingConfigurerImpl.ManagerMeta> = ArrayList()

        configurers.forEach { configurer ->
            val meta = configurer.createMeta()

            if (meta == null) {
                log.warn("Warning '${configurer::class.simpleName}' configuration is defective configurer will be ignored")

                return@forEach
            }

            log.debug("Configuration for '${configurer::class.simpleName}' is valid")
            validMeta.add(meta)
        }

        return validMeta
    }
}