package com.vanadis.core.configuration.manager.events

import com.vanadis.core.command.provider.CommandProvider

// TODO Migrate to annotations

interface BeforeCommandsInitListener {

    fun beforeCommandsInit()
}

interface AfterCommandsInitListener {

    fun afterCommandsInit()
}

interface BeforeInitCommandsListener<T: Any> {

    fun handleUninitializedCommand(command: CommandProvider<T>)
}

interface AfterInitCommandsListener<T: Any> {

    fun handleInitializedCommand(command: CommandProvider<T>)
}

interface AllCommandListener<T: Any> {

    fun handleCommands(commands: Collection<CommandProvider<T>>)
}