package com.vanadis.core.configuration.manager.settings.handling

import com.vanadis.core.configuration.manager.settings.ManagerConfigurer
import com.vanadis.core.command.provider.CommandProvider
import kotlin.reflect.KClass

interface CommandsHandlingConfigurer {

    fun addCommandAnnotation(annotationClass: KClass<out Annotation>): CommandsHandlingConfigurer

    fun addCommandAnnotation(vararg annotationClass: KClass<out Annotation>): CommandsHandlingConfigurer

    fun setCommandProvider(commandProvider: KClass<out CommandProvider<*>>) : CommandsHandlingConfigurer

    fun setManager(commandsManager: KClass<*>) : CommandsHandlingConfigurer

    fun setManager(commandsManager: KClass<*>, commandClass: KClass<*>) : CommandsHandlingConfigurer

    fun and() : ManagerConfigurer
}