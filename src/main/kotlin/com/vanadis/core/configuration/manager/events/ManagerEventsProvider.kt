package com.vanadis.core.configuration.manager.events

import com.vanadis.core.configuration.manager.settings.handling.CommandsHandlingConfigurerImpl
import com.vanadis.core.command.provider.CommandProvider
import com.vanadis.logger.logback.logger
import org.springframework.beans.factory.config.AutowireCapableBeanFactory
import java.lang.IllegalStateException
import java.lang.RuntimeException
import java.lang.reflect.InvocationTargetException
import kotlin.reflect.KCallable
import kotlin.reflect.KClass

internal class ManagerEventsProvider(
        managerMeta: CommandsHandlingConfigurerImpl.ManagerMeta,
        private val managerInstance: Any,
        private val commandProvider: KClass<out CommandProvider<*>>,
        private val autowireCapableBeanFactory: AutowireCapableBeanFactory
) {

    companion object {

        @JvmStatic
        private val log = logger()
    }

    private val commandClass = managerMeta.commandClass

    private val hasBeforeInitHandler = managerInstance is BeforeInitCommandsListener<*>

    private val hasAfterInitHandler = managerInstance is AfterInitCommandsListener<*>

    private val hasAllHandler = managerInstance is AllCommandListener<*>

    private val hasBeforeCommandsInitHandler = managerInstance is BeforeCommandsInitListener

    private val hasAfterCommandsInitHandler = managerInstance is AfterCommandsInitListener

    private val allCommands = HashMap<Any, CommandProvider<*>>()

    fun notifyBeforeCommandsInit() {
        if (hasBeforeCommandsInitHandler) {
            val casted = managerInstance as BeforeCommandsInitListener
            casted.beforeCommandsInit()
        }
    }

    fun notifyAfterCommandsInit() {
        if (hasAfterCommandsInitHandler) {
            val casted = managerInstance as AfterCommandsInitListener
            casted.afterCommandsInit()
        }
    }

    fun notifyBeforeCommandInitialization(annotatedBean: Any) {
        if (!commandClass.isInstance(annotatedBean)) {
            return
        }

        // Always call getProvider otherwise you can skip the step of adding command class
        //  to commands list
        val provider = getProvider(annotatedBean)

        if (hasBeforeInitHandler) {
            val casted = managerInstance as BeforeInitCommandsListener<*>
            invokeEventMethodWithArgument(casted::handleUninitializedCommand, provider)
        }
    }

    fun notifyAfterCommandInitialization(annotatedBean: Any) {
        // Always call getProvider otherwise you can skip the step of adding command class
        //  to commands list
        val provider = allCommands[annotatedBean] ?: getProvider(annotatedBean)

        if (hasAfterInitHandler && commandClass.isInstance(annotatedBean)) {
            val casted = managerInstance as AfterInitCommandsListener<*>
            invokeEventMethodWithArgument(casted::handleInitializedCommand, provider)
        }
    }

    fun notifyAllCommandsInitialized() {
        if (hasAllHandler) {
            val casted = managerInstance as AllCommandListener<*>
            invokeEventMethodWithArgument(casted::handleCommands, allCommands.values)
        }
    }

    private fun getProvider(commandInstance: Any): CommandProvider<*> {
        var provider = allCommands[commandInstance]

        if (provider == null) {
            // TODO Here can be added support of constructor arguments injection
            val constructor = commandProvider.constructors
                    .find { it.parameters.size == 1 }
                    ?: throw IllegalStateException("Constructor with 1 argument not found in provider `$commandProvider`!")

            provider = constructor.call(commandInstance)
            autowireCapableBeanFactory.autowireBean(provider)
            allCommands[commandInstance] = provider
        }

        return provider
    }

    private fun invokeEventMethodWithArgument(eventMethod: KCallable<*>, argument: Any) {
        try {
            eventMethod.call(argument)
        } catch (ioe: InvocationTargetException) {
            throw RuntimeException("Type mismatch in handler " +
                    "'${managerInstance::class.qualifiedName}.${eventMethod.name}(...)' and in CommandsManager " +
                    "configuration. Expected type is '${commandClass.qualifiedName}'!", ioe)
        }
    }
}