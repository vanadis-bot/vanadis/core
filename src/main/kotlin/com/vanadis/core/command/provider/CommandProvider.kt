package com.vanadis.core.command.provider

abstract class CommandProvider<T: Any> constructor(
        val commandInstance: T
) {
}