package com.vanadis.core.command.annotation

@Retention(AnnotationRetention.RUNTIME)
@Target(AnnotationTarget.FUNCTION)
annotation class CommandHandler (
    val argumentsPattern: String = ""
)